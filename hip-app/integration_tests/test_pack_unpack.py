import os
import tempfile
import random

import pytest

from utils import shell, same_files


def test_pack_unpack_small(temp_dir, temp_file, other_temp_file) -> None:
    with open(temp_file, 'wt') as h:
        h.write('hello world!\n')
    shell(f"hip pack -i imgs -d {temp_file} -o {temp_dir}")
    assert len(os.listdir(temp_dir)) == 1
    shell(f"hip unpack -i {temp_dir} -o {other_temp_file}")
    result = open(other_temp_file).read()
    assert result.strip() == 'hello world!'


def test_pack_unpack_big(temp_dir, temp_file, other_temp_file) -> None:
    shell(f"hip pack -i imgs -d small_pic.png -o {temp_dir}")
    assert len(os.listdir(temp_dir)) == 2
    shell(f"hip unpack -i {temp_dir} -o {other_temp_file}")
    same_files(other_temp_file, 'small_pic.png')


def test_pack_unpack_several_bits(temp_dir, temp_file) -> None:
    shell(f"hip pack -i imgs/1_face.png -d imgs/0_picture.png -o {temp_dir} -b 3")
    assert len(os.listdir(temp_dir)) == 1
    shell(f"hip unpack -i {temp_dir} -o {temp_file}")
    same_files(temp_file, 'imgs/0_picture.png')
