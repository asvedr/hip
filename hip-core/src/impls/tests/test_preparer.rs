use crate::entities::common::{Codec, DataHeader, MarkedData};
use crate::{CodecError, HIPError};
use rstest::*;

use crate::impls::codecs;
use crate::impls::preparer::Preparer;
use crate::proto::common::{ICodec, IPreparer, MockICodec};

#[fixture]
fn real_preparer() -> Preparer {
    Preparer {
        codecs: codecs::all(),
    }
}

fn mock_preparer(results: Vec<(Codec, Result<Vec<u8>, CodecError>)>) -> Preparer {
    let codecs = results
        .into_iter()
        .map(|(slug, response)| -> Box<dyn ICodec> {
            let mut codec = MockICodec::new();
            codec.expect_slug().returning(move || slug);
            let response_copy = response.clone();
            codec
                .expect_encode()
                .returning(move |_| response_copy.clone());
            codec.expect_decode().returning(move |_| response.clone());
            Box::new(codec)
        })
        .collect();
    Preparer { codecs }
}

#[rstest(
    codecs,
    used,
    case(
        vec![(Codec::None, Ok(vec![1, 2])), (Codec::Brotli, Ok(vec![1]))],
        1,
    ),
    case(
        vec![(Codec::None, Ok(vec![2])), (Codec::Brotli, Ok(vec![1, 2]))],
        0,
    ),
    case(
        vec![(Codec::None, Ok(vec![1, 2])), (Codec::Brotli, Err(CodecError::CodecFailed("".to_string())))],
        0,
    ),
)]
fn test_mocked_codecs_prepare(codecs: Vec<(Codec, Result<Vec<u8>, CodecError>)>, used: usize) {
    let data: &[u8] = &[1, 2, 3];
    let (codec_used, res_data) = codecs[used].clone();
    let res_data = res_data.unwrap();
    let preparer = mock_preparer(codecs);
    let prepared = preparer.prepare(data, 1);
    assert_eq!(prepared.data, res_data);
    assert_eq!(prepared.header.codec, codec_used);
    assert_eq!(prepared.header.data_len, res_data.len());
}

#[rstest(
    source, codec, data,
    case(b"hello world", Codec::None, b"hello world"),
    case(
        b"hello hello hello hello hello",
        Codec::Brotli,
        &[23, 28, 0, 248, 141, 148, 110, 222, 68, 85, 134, 150, 108, 32, 111, 65, 79, 28, 32, 113],
    )
)]
fn test_real_codec_ok(real_preparer: Preparer, source: &[u8], codec: Codec, data: &[u8]) {
    let result = real_preparer.prepare(source, 1);
    assert_eq!(
        result.header,
        DataHeader {
            has_next: false,
            bits_used: 1,
            data_len: data.len(),
            codec
        }
    );
    assert_eq!(result.data, data);

    let decoded = real_preparer.decode(result).unwrap();
    assert_eq!(decoded, source);
}

#[rstest]
fn test_real_codec_error(real_preparer: Preparer) {
    let source = MarkedData {
        header: DataHeader {
            has_next: false,
            bits_used: 1,
            data_len: 3,
            codec: Codec::Brotli,
        },
        data: vec![1, 2, 3],
    };
    let err = real_preparer.decode(source).unwrap_err();
    assert_eq!(
        err,
        HIPError::CanNotDecode("codec(Brotli): Invalid Data".to_string())
    );
}
