use clidi::entities::{CliUseCaseMeta, RawUseCaseRequest, UseCaseError};
use clidi::proto::cli::ICliUseCase;
use clidi::proto::di::IUseCase;

pub struct GetVersionUseCase {}

impl GetVersionUseCase {
    pub fn new() -> GetVersionUseCase {
        GetVersionUseCase {}
    }
}

impl IUseCase for GetVersionUseCase {
    type Request = ();
    type Response = ();

    fn execute(&mut self, _: ()) -> Result<(), UseCaseError> {
        println!("author: asvedr");
        println!("version: {}", env!("CARGO_PKG_VERSION"));
        Ok(())
    }
}

impl ICliUseCase for GetVersionUseCase {
    fn get_meta(&self) -> CliUseCaseMeta {
        CliUseCaseMeta::new("get information about build")
    }

    fn validate_request(&self, _: RawUseCaseRequest) -> Self::Request {}
}
