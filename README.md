# Hide In Pic

## What is it
Simple steganography tool and lib

## Tool
The `hip-app` dir is a CLI tool for the steganography.
First install `cargo` from https://www.rust-lang.org/

### Test:
```
cd hip-app
cargo test
sh int_test.sh
```

### Install:
```sh
cargo build --release
cp target/release/hip-app /usr/bin/hip
```

### Usage:
- check `hip -h` for help
- `hip pack` - put data inside one or several pictures
- WARNING compressed formats like JPEG will damage data
- `hip unpack` - extract data from image(s)

## Lib

The `hip-core` dir is a rust lib for hip-app features.

### Usage:

pack:
```rust
use hip_core::{
  IComposer, IImagePack, IPreparer, MarkedData,
  create_composer, create_image_pack, create_preparer
};

let data: &[u8] = ...;
// first prepare
let preparer = create_preparer();
let use_bits = 1; // 1 ..= 8
let prepared: MarkedData = preparer.prepare(&data, use_bits);
// If you need to separate to to several blocks:
let composer = create_composer();
let (first, last) = composer.separate(prepared, 100_000)?;
// put data
let packer = create_image_pack();
let new_pic = packer.pack(pic, &data)?;
// then save it like regular image
new_pic.save_with_format(path, format)
```

unpack:
```rust
use image::DynamicImage;
use hip_core::{
  IComposer, IImagePack, IPreparer, MarkedData,
  create_composer, create_image_pack, create_preparer
};

let img: DynamicImage = ...;
// first extract
let packer = create_image_pack();
let data: PrepatedData = packer.unpack(img)?;
// If you need to join separated
let composer = create_composer();
let parts: Vec<PreparedData> = ...;
let joined = composer.compose(parts)?;
// decode it
let preparer = create_preparer();
let result: Vec<u8> = preparer.decode(data)?;
```
