mod application;
mod entities;
mod impls;
mod proto;
mod use_cases;

use clidi::cli::{Runner, RunnerTree};

fn main() {
    let pack = Runner::new_leaf(Box::new(application::pack), "put data in image(s)", "pack");
    let unpack = Runner::new_leaf(
        Box::new(application::unpack),
        "extract data from image(s)",
        "unpack",
    );
    let scan = Runner::new_leaf(
        Box::new(application::scan),
        "scan image(s) for data",
        "scan",
    );
    let version = Runner::new_leaf(Box::new(application::version), "show version", "version");
    let tree = RunnerTree::fold(
        vec![pack, unpack, scan, version],
        "simple steganography tool",
    );
    tree.into_main_runner().run()
}
