#[derive(Debug, Clone, Eq, PartialEq)]
pub enum HIPError {
    ContainerTooSmall,
    CanNotDecode(String),
    UnknownCodec(String),
    InvalidHeader,
    UnexpectedEOF,
    MergingDifferentCodecs,
    InvalidBitNumber,
}

#[derive(Debug, Clone)]
pub enum CodecError {
    CodecFailed(String),
    CanNotDecode(String),
}

impl ToString for HIPError {
    fn to_string(&self) -> String {
        format!("{:?}", self)
    }
}
