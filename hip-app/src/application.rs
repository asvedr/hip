use clidi::entities::ConfigError;
use hip_core::{create_composer, create_image_pack, create_preparer};

use crate::impls::file_walker::FileWalker;
use crate::proto::common::IFileWalker;
use crate::use_cases::pack::PackUseCase;
use crate::use_cases::scan::ScanUseCase;
use crate::use_cases::unpack::UnpackUseCase;
use crate::use_cases::version::GetVersionUseCase;

fn file_walker() -> Box<dyn IFileWalker> {
    Box::new(FileWalker::new())
}

pub fn pack() -> Result<PackUseCase, ConfigError> {
    Ok(PackUseCase::new(
        file_walker(),
        create_preparer(),
        create_composer(),
        create_image_pack(),
    ))
}

pub fn unpack() -> Result<UnpackUseCase, ConfigError> {
    Ok(UnpackUseCase::new(
        file_walker(),
        create_preparer(),
        create_composer(),
        create_image_pack(),
    ))
}

pub fn scan() -> Result<ScanUseCase, ConfigError> {
    Ok(ScanUseCase::new(file_walker(), create_image_pack()))
}

pub fn version() -> Result<GetVersionUseCase, ConfigError> {
    Ok(GetVersionUseCase::new())
}
