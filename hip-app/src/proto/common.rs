use std::io;
use std::path::{Path, PathBuf};

pub trait IFileWalker {
    fn get_all_files(&self, base_dir: &Path) -> io::Result<Vec<PathBuf>>;
    fn read_file(&self, path: &Path) -> io::Result<Vec<u8>>;
    fn write_file(&self, path: &Path, data: &[u8]) -> io::Result<()>;
    fn create_dir(&self, path: &Path) -> io::Result<()>;

    fn get_all_files_sorted(&self, base_dir: &Path) -> io::Result<Vec<PathBuf>> {
        let mut files = self.get_all_files(base_dir)?;
        files.sort();
        Ok(files)
    }
}
