use clidi::cli::validators::AsIsValidator;
use clidi::entities::{CliUseCaseMeta, Param, RawUseCaseRequest, UseCaseError};
use clidi::proto::cli::ICliUseCase;
use clidi::proto::di::IUseCase;
use hip_core::{DataHeader, IImagePack};
use image::open;
use std::path::{Path, PathBuf};

use crate::proto::common::IFileWalker;

pub struct ScanUseCase {
    file_walker: Box<dyn IFileWalker>,
    hip_pack: Box<dyn IImagePack>,
}

pub struct Request {
    imgs_path: PathBuf,
}

impl ScanUseCase {
    pub fn new(file_walker: Box<dyn IFileWalker>, hip_pack: Box<dyn IImagePack>) -> ScanUseCase {
        Self {
            file_walker,
            hip_pack,
        }
    }

    fn get_header(&self, path: &Path) -> Result<Option<DataHeader>, UseCaseError> {
        let img = open(path).map_err(UseCaseError::make_runtime)?;
        Ok(self.hip_pack.scan_header(img))
    }

    fn show_header(&self, path: &Path, header: DataHeader) {
        println!("FOUND (path={}): {:?}", path.to_str().unwrap(), header)
    }
}

impl IUseCase for ScanUseCase {
    type Request = Request;
    type Response = ();

    fn execute(&mut self, request: Self::Request) -> Result<Self::Response, UseCaseError> {
        let img_paths = self
            .file_walker
            .get_all_files_sorted(&request.imgs_path)
            .map_err(UseCaseError::make_runtime)?;
        for img in img_paths {
            if let Some(header) = self.get_header(&img)? {
                self.show_header(&img, header)
            }
        }
        Ok(())
    }
}

impl ICliUseCase for ScanUseCase {
    fn get_meta(&self) -> CliUseCaseMeta {
        CliUseCaseMeta::new("check image(s) for data")
            .param(Param::new::<_, AsIsValidator>("imgs").short("i"))
    }

    fn validate_request(&self, raw: RawUseCaseRequest) -> Self::Request {
        Request {
            imgs_path: PathBuf::from(raw.get::<String>("imgs")),
        }
    }
}
