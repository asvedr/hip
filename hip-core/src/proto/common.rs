use image::DynamicImage;

#[cfg(test)]
use mockall::automock;

use crate::entities::common::{Codec, DataHeader, MarkedData};
use crate::{CodecError, HIPError};

pub trait IImagePack {
    fn get_picture_bytes_capacity(&self, image: &DynamicImage, bits_used: usize) -> usize;
    fn pack(&self, image: DynamicImage, data: &MarkedData) -> Result<DynamicImage, HIPError>;
    fn unpack(&self, image: DynamicImage) -> Result<MarkedData, HIPError>;
    fn scan_header(&self, image: DynamicImage) -> Option<DataHeader>;
}

#[cfg_attr(test, automock)]
pub trait ICodec {
    fn slug(&self) -> Codec;
    fn encode(&self, data: &[u8]) -> Result<Vec<u8>, CodecError>;
    fn decode(&self, data: &[u8]) -> Result<Vec<u8>, CodecError>;
}

pub trait IPreparer {
    fn prepare(&self, data: &[u8], bits_used: usize) -> MarkedData;
    fn decode(&self, data: MarkedData) -> Result<Vec<u8>, HIPError>;
}

pub trait IComposer {
    fn compose(&self, parts: Vec<MarkedData>) -> Result<MarkedData, HIPError>;
    fn separate(
        &self,
        source: MarkedData,
        limit: usize,
    ) -> Result<(MarkedData, Option<MarkedData>), HIPError>;
}
