use image::{open, DynamicImage, ImageFormat};
use rstest::*;
use tempdir::TempDir;

use crate::entities::common::{Codec, DataHeader};
use crate::impls::image_pack::ImagePack;
use crate::{HIPError, IImagePack, MarkedData};

#[fixture]
fn image_colors() -> DynamicImage {
    open("test_data/picture.png").unwrap()
}

#[fixture]
fn image_face() -> DynamicImage {
    open("test_data/face.png").unwrap()
}

#[fixture]
fn dir() -> TempDir {
    TempDir::new("tests").unwrap()
}

#[fixture]
fn packer() -> ImagePack {
    ImagePack::new()
}

#[rstest(bit_used, result, case(1, 209607), case(2, 419204))]
fn test_capacity(image_face: DynamicImage, packer: ImagePack, bit_used: usize, result: usize) {
    let capacity = packer.get_picture_bytes_capacity(&image_face, bit_used);
    assert_eq!(capacity, result)
}

#[rstest(bits_used, case(1), case(2), case(3))]
fn test_read_pack_unpack(image_face: DynamicImage, packer: ImagePack, bits_used: usize) {
    let message: &[u8] = b"Hello my friends! I'm glad to see you!";
    let marked = MarkedData {
        header: DataHeader {
            has_next: false,
            bits_used,
            data_len: message.len(),
            codec: Codec::None,
        },
        data: message.to_vec(),
    };
    let packed = packer.pack(image_face, &marked).unwrap();
    let unpacked = packer.unpack(packed).unwrap();
    assert_eq!(unpacked.header, marked.header);
    assert_eq!(unpacked.data, marked.data);
}

#[rstest(bits_used, case(1), case(2), case(3))]
fn test_save_restore_png(
    image_face: DynamicImage,
    packer: ImagePack,
    dir: TempDir,
    bits_used: usize,
) {
    let message: &[u8] = b"Hello my friends! I'm glad to see you!";
    let marked = MarkedData {
        header: DataHeader {
            has_next: false,
            bits_used,
            data_len: message.len(),
            codec: Codec::None,
        },
        data: message.to_vec(),
    };
    let packed = packer.pack(image_face, &marked).unwrap();
    let name = "result.png";
    let res_name = dir.path().join(name);
    packed
        .save_with_format(&res_name, ImageFormat::PNG)
        .unwrap();

    let pic = open(res_name).unwrap();
    let unpacked = packer.unpack(pic).unwrap();
    assert_eq!(unpacked.header, marked.header);
    assert_eq!(unpacked.data, marked.data);
}

#[rstest]
fn test_save_restore_jpg_failed(image_face: DynamicImage, packer: ImagePack, dir: TempDir) {
    let message: &[u8] = b"Hello my friends! I'm glad to see you!";
    let marked = MarkedData {
        header: DataHeader {
            has_next: false,
            bits_used: 1,
            data_len: message.len(),
            codec: Codec::None,
        },
        data: message.to_vec(),
    };
    let packed = packer.pack(image_face, &marked).unwrap();
    let name = "result.jpg";
    let res_name = dir.path().join(name);
    packed
        .save_with_format(&res_name, ImageFormat::JPEG)
        .unwrap();
    let pic = open(res_name).unwrap();
    let err = match packer.unpack(pic) {
        Ok(_) => panic!(),
        Err(val) => val,
    };
    assert_eq!(err, HIPError::InvalidHeader);
}

#[rstest]
fn test_data_too_big(image_face: DynamicImage, packer: ImagePack) {
    let message: &[u8] = &[0; 209_777];
    let marked = MarkedData {
        header: DataHeader {
            has_next: false,
            bits_used: 1,
            data_len: message.len(),
            codec: Codec::None,
        },
        data: message.to_vec(),
    };
    let err = match packer.pack(image_face, &marked) {
        Ok(_) => panic!(),
        Err(val) => val,
    };
    assert_eq!(err, HIPError::ContainerTooSmall);
}

#[rstest]
fn test_scan_empty(image_face: DynamicImage, packer: ImagePack) {
    assert!(packer.scan_header(image_face).is_none())
}

#[rstest(bits_used, case(1), case(2))]
fn test_scan_found(image_face: DynamicImage, packer: ImagePack, bits_used: usize) {
    let message: &[u8] = b"Hello my friends! I'm glad to see you!";
    let marked = MarkedData {
        header: DataHeader {
            has_next: false,
            bits_used,
            data_len: message.len(),
            codec: Codec::None,
        },
        data: message.to_vec(),
    };
    let packed = packer.pack(image_face, &marked).unwrap();
    let found = packer.scan_header(packed).unwrap();
    assert_eq!(
        found,
        DataHeader {
            has_next: false,
            bits_used,
            data_len: message.len(),
            codec: Codec::None
        }
    )
}
