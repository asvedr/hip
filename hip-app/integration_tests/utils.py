import subprocess as sp
from filecmp import dircmp


def shell(command: str, get_out: bool = False) -> str:
    if get_out:
        return sp.check_output(["/bin/sh", "-c", command]).decode()
    else:
        sp.run(["/bin/sh", "-c", command], check=True)
        return ''


def same_files(a: str, b: str) -> None:
    assert open(a, "rb").read() == open(b, "rb").read()


def diff_files(a: str, b: str) -> None:
    assert open(a, "rb").read() != open(b, "rb").read()


def equal_dirs(dir_a: str, dir_b: str):
    diff = dircmp(dir_a, dir_b)
    ok = True
    if diff.left_only:
        print("LEFT ONLY:", diff.left_only)
        ok = False
    if diff.right_only:
        print("RIGHT ONLY:", diff.right_only)
        ok = False
    if diff.diff_files:
        print("DIFF FILES:", diff.diff_files)
        ok = False
    assert ok

