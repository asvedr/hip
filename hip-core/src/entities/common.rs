use std::mem::size_of;
use std::ops::{BitAnd, BitOr};

#[derive(Clone, Copy, Eq, PartialEq, Debug)]
pub enum Codec {
    None,
    Brotli,
}

pub const ALL_CODECS: &[Codec] = &[Codec::None, Codec::Brotli];

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct DataHeader {
    pub has_next: bool,
    pub bits_used: usize,
    pub data_len: usize,
    pub codec: Codec,
}

#[derive(Clone, Eq, PartialEq)]
pub struct MarkedData {
    pub(crate) header: DataHeader,
    pub(crate) data: Vec<u8>,
}

type Len = u64;

impl Codec {
    pub fn to_u8(self) -> u8 {
        #[allow(clippy::needless_range_loop)]
        for i in 0..ALL_CODECS.len() {
            if ALL_CODECS[i] == self {
                return (i as u8).to_be();
            }
        }
        unreachable!()
    }

    pub fn from_u8(data: u8) -> Option<Self> {
        let index = u8::from_be(data) as usize;
        if index <= ALL_CODECS.len() {
            Some(ALL_CODECS[index])
        } else {
            None
        }
    }
}

impl DataHeader {
    const HAS_NEXT: u8 = u8::from_be(0b1000_0000);
    const HAS_NEXT_MASK: u8 = u8::from_be(0b0111_1111);

    pub fn len() -> usize {
        size_of::<Len>() + 2
    }

    pub fn to_bytes(&self) -> Vec<u8> {
        let mut data = (self.data_len as Len).to_be_bytes().to_vec();
        data.push(self.codec.to_u8());
        let mut flags = (self.bits_used as u8).to_be();
        if self.has_next {
            flags = flags.bitor(Self::HAS_NEXT);
        }
        data.push(flags);
        data
    }

    pub fn from_bytes(bytes: &[u8]) -> Option<Self> {
        let shift = size_of::<Len>();
        let len_slice = &bytes[..shift];
        let data_len = Len::from_be_bytes(len_slice.try_into().unwrap());
        let codec = Codec::from_u8(bytes[shift])?;
        let flags = bytes[shift + 1];
        let has_next = flags.bitand(Self::HAS_NEXT) != 0;
        let bits_used = u8::from_be(flags.bitand(Self::HAS_NEXT_MASK)) as usize;
        Some(Self {
            has_next,
            data_len: data_len as usize,
            codec,
            bits_used,
        })
    }
}

impl MarkedData {
    #[allow(clippy::len_without_is_empty)]
    pub fn len(&self) -> usize {
        DataHeader::len() + self.data.len()
    }

    pub fn has_next(&self) -> bool {
        self.header.has_next
    }
}
