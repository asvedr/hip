use image::{Bgr, Bgra, Luma, LumaA, Rgb, Rgba};

pub trait PixelContainer {
    const CAPACITY: usize;
    // fn pack(&self, value: u8, index: usize) -> Self;
    // fn unpack(&self) -> [u8; Self::CAPACITY];
    //
    // fn set(src: u8, value: u8) -> u8 {
    //     if value {
    //         src.bitor(ONE)
    //     } else {
    //         src.bitand(ZERO)
    //     }
    // }
    //
    // fn get(src: u8) -> u8 {
    //     src.bitand(ONE)
    // }
}

impl PixelContainer for Luma<u8> {
    const CAPACITY: usize = 1;

    // fn pack(&self, value: u8, index: usize) -> Self {
    //     Luma([Self::set(self[index], value)])
    // }
    //
    // fn unpack(&self) -> [u8; Self::CAPACITY] {
    //     [self.get(self[0])]
    // }
}

impl PixelContainer for LumaA<u8> {
    const CAPACITY: usize = 2;

    // fn pack(&self, value: u8, index: usize) -> Self {
    //     let mut copy = self.clone();
    //     copy[index] = Self::set(self[index], value);
    //     copy
    // }
    //
    // fn unpack(&self) -> [u8; Self::CAPACITY] {
    //     [self.get(self[0]), self.get(self[1])]
    // }
}

impl PixelContainer for Rgb<u8> {
    const CAPACITY: usize = 3;

    // fn pack(&self, value: u8, index: usize) -> Self {
    //     let mut copy = self.clone();
    //     copy[index] = Self::set(self[index], value);
    //     copy
    // }
    //
    // fn unpack(&self) -> [u8; Self::CAPACITY] {
    //     [self.get(self[0]), self.get(self[1]), self.get(self[2])]
    // }
}

impl PixelContainer for Bgr<u8> {
    const CAPACITY: usize = 3;

    // fn pack(&self, value: u8, index: usize) -> Self {
    //     let mut copy = self.clone();
    //     copy[index] = Self::set(self[index], value);
    //     copy
    // }
    //
    // fn unpack(&self) -> [u8; Self::CAPACITY] {
    //     [self.get(self[0]), self.get(self[1]), self.get(self[2])]
    // }
}

impl PixelContainer for Rgba<u8> {
    const CAPACITY: usize = 4;

    // fn pack(&self, value: u8, index: usize) -> Self {
    //     let mut copy = self.clone();
    //     copy[index] = Self::set(self[index], value);
    //     copy
    // }
    //
    // fn unpack(&self) -> [u8; Self::CAPACITY] {
    //     [self.get(self[0]), self.get(self[1]), self.get(self[2]), self.get(self[3])]
    // }
}

impl PixelContainer for Bgra<u8> {
    const CAPACITY: usize = 4;

    // fn pack(&self, value: u8, index: usize) -> Self {
    //     let mut copy = self.clone();
    //     copy[index] = Self::set(self[index], value);
    //     copy
    // }
    //
    // fn unpack(&self) -> [u8; Self::CAPACITY] {
    //     [self.get(self[0]), self.get(self[1]), self.get(self[2]), self.get(self[3])]
    // }
}
