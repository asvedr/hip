import tempfile
from typing import Generator

import pytest


@pytest.fixture
def temp_dir() -> Generator[str, None, None]:
    tdir = tempfile.TemporaryDirectory()
    yield tdir.__enter__()
    tdir.__exit__(None, None, None)


@pytest.fixture
def temp_file() -> Generator[str, None, None]:
    tfile = tempfile.NamedTemporaryFile()
    yield tfile.__enter__().name
    tfile.__exit__(None, None, None)


@pytest.fixture
def other_temp_file() -> Generator[str, None, None]:
    tfile = tempfile.NamedTemporaryFile()
    yield tfile.__enter__().name
    tfile.__exit__(None, None, None)
