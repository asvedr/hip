use crate::entities::common::{DataHeader, MarkedData};
use crate::proto::common::IComposer;
use crate::HIPError;

pub struct Composer {}

impl IComposer for Composer {
    fn compose(&self, parts: Vec<MarkedData>) -> Result<MarkedData, HIPError> {
        let mut result = MarkedData {
            header: DataHeader {
                bits_used: parts[0].header.bits_used,
                has_next: false,
                data_len: 0,
                codec: parts[0].header.codec,
            },
            data: vec![],
        };
        for mut part in parts {
            if result.header.codec != part.header.codec {
                return Err(HIPError::MergingDifferentCodecs);
            }
            result.data.append(&mut part.data);
        }
        result.header.data_len = result.data.len();
        Ok(result)
    }

    fn separate(
        &self,
        source: MarkedData,
        limit: usize,
    ) -> Result<(MarkedData, Option<MarkedData>), HIPError> {
        if source.len() <= limit {
            return Ok((source, None));
        }
        if limit < DataHeader::len() + 1 {
            return Err(HIPError::ContainerTooSmall);
        }
        let split_on = limit - DataHeader::len();
        let first = MarkedData {
            header: DataHeader {
                bits_used: source.header.bits_used,
                has_next: true,
                data_len: split_on,
                codec: source.header.codec,
            },
            data: source.data[..split_on].to_vec(),
        };
        let new_data = source.data[split_on..].to_vec();
        let second = MarkedData {
            header: DataHeader {
                bits_used: source.header.bits_used,
                has_next: false,
                data_len: new_data.len(),
                codec: source.header.codec,
            },
            data: new_data,
        };
        Ok((first, Some(second)))
    }
}
