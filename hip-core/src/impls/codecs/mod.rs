use crate::proto::common::ICodec;

mod brotli;
mod none;

pub fn all() -> Vec<Box<dyn ICodec>> {
    vec![Box::new(none::Codec {}), Box::new(brotli::Codec {})]
}
