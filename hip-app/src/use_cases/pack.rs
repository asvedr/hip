use clidi::cli::validators::{AsIsValidator, FromStrValidator};
use clidi::entities::{CliUseCaseMeta, Param, RawUseCaseRequest, UseCaseError};
use clidi::proto::cli::ICliUseCase;
use clidi::proto::di::IUseCase;
use hip_core::{IComposer, IImagePack, IPreparer, MarkedData};
use image::{guess_format, load_from_memory_with_format, open, DynamicImage, ImageFormat};
use std::path::{Path, PathBuf};

use crate::proto::common::IFileWalker;

pub struct PackUseCase {
    file_walker: Box<dyn IFileWalker>,
    hip_preparer: Box<dyn IPreparer>,
    hip_composer: Box<dyn IComposer>,
    hip_pack: Box<dyn IImagePack>,
}

pub struct Request {
    data_path: PathBuf,
    imgs_path: PathBuf,
    out_path: PathBuf,
    bits: usize,
}

impl PackUseCase {
    pub fn new(
        file_walker: Box<dyn IFileWalker>,
        hip_preparer: Box<dyn IPreparer>,
        hip_composer: Box<dyn IComposer>,
        hip_pack: Box<dyn IImagePack>,
    ) -> PackUseCase {
        Self {
            file_walker,
            hip_preparer,
            hip_composer,
            hip_pack,
        }
    }

    fn verify(&self, path: PathBuf, expected: MarkedData) -> Result<(), UseCaseError> {
        let img = open(path).unwrap();
        let got = match self.hip_pack.unpack(img) {
            Ok(val) => val,
            Err(_) => return UseCaseError::raise_runtime("container validation failed"),
        };
        if got != expected {
            return UseCaseError::raise_runtime("container validation failed");
        }
        Ok(())
    }

    fn save(
        &self,
        original_path: PathBuf,
        format: ImageFormat,
        pic: DynamicImage,
        data: MarkedData,
        out_dir: &Path,
    ) -> Result<PathBuf, UseCaseError> {
        let new_img = self
            .hip_pack
            .pack(pic, &data)
            .map_err(UseCaseError::make_runtime)?;
        let new_path = out_dir.join(original_path.file_name().unwrap());
        new_img
            .save_with_format(&new_path, format)
            .map_err(UseCaseError::make_runtime)?;
        Ok(new_path)
    }

    fn pack_part(
        &self,
        path: PathBuf,
        data: MarkedData,
        out_dir: &Path,
        bits: usize,
    ) -> Result<Option<MarkedData>, UseCaseError> {
        self.file_walker
            .create_dir(out_dir)
            .map_err(UseCaseError::make_runtime)?;
        let pic_data = self
            .file_walker
            .read_file(&path)
            .map_err(UseCaseError::make_runtime)?;
        let format = match guess_format(&pic_data) {
            Ok(val) => val,
            Err(_) => {
                eprintln!("skip file: {}", path.to_str().unwrap());
                return Ok(Some(data));
            }
        };
        let pic =
            load_from_memory_with_format(&pic_data, format).map_err(UseCaseError::make_runtime)?;
        let capacity = self.hip_pack.get_picture_bytes_capacity(&pic, bits);
        let (now, next) = self
            .hip_composer
            .separate(data, capacity)
            .map_err(UseCaseError::make_runtime)?;
        let filename = self.save(path, format, pic, now.clone(), out_dir)?;
        self.verify(filename, now)?;
        Ok(next)
    }
}

impl IUseCase for PackUseCase {
    type Request = Request;
    type Response = ();

    fn execute(&mut self, request: Self::Request) -> Result<Self::Response, UseCaseError> {
        let data = self
            .file_walker
            .read_file(&request.data_path)
            .map_err(UseCaseError::make_runtime)?;
        let img_paths = self
            .file_walker
            .get_all_files_sorted(&request.imgs_path)
            .map_err(UseCaseError::make_runtime)?;
        let mut marked_data = self.hip_preparer.prepare(&data, request.bits);
        for img in img_paths {
            match self.pack_part(img, marked_data, &request.out_path, request.bits)? {
                Some(rest) => marked_data = rest,
                None => {
                    println!("success");
                    return Ok(());
                }
            }
        }
        Err(UseCaseError::make_runtime("container is too small"))
    }
}

impl ICliUseCase for PackUseCase {
    fn get_meta(&self) -> CliUseCaseMeta {
        CliUseCaseMeta::new("pack data inside image(s)")
            .param(Param::new::<_, AsIsValidator>("data").short("d"))
            .param(Param::new::<_, AsIsValidator>("imgs").short("i"))
            .param(Param::new::<_, AsIsValidator>("out").short("o"))
            .param(
                Param::new::<_, FromStrValidator<usize>>("bits")
                    .short("b")
                    .default("1"),
            )
    }

    fn validate_request(&self, raw: RawUseCaseRequest) -> Self::Request {
        Request {
            data_path: PathBuf::from(raw.get::<String>("data")),
            imgs_path: PathBuf::from(raw.get::<String>("imgs")),
            out_path: PathBuf::from(raw.get::<String>("out")),
            bits: raw.get("bits"),
        }
    }
}
