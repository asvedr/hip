use rstest::*;

use crate::entities::common::{Codec, DataHeader};
use crate::impls::composer::Composer;
use crate::{HIPError, IComposer, MarkedData};

#[fixture]
fn composer() -> Composer {
    Composer {}
}

#[fixture]
fn data() -> MarkedData {
    MarkedData {
        header: DataHeader {
            has_next: false,
            bits_used: 1,
            data_len: 100,
            codec: Codec::None,
        },
        data: (1..=100).collect::<Vec<u8>>(),
    }
}

#[rstest]
fn test_separate_error(composer: Composer, data: MarkedData) {
    let err = composer.separate(data, 5).err().unwrap();
    assert_eq!(err, HIPError::ContainerTooSmall);
}

#[rstest]
fn test_separate_split(composer: Composer, data: MarkedData) {
    let (block_a, block_b) = composer.separate(data, 50).unwrap();
    assert_eq!(block_a.len(), 50);
    assert_eq!(block_a.header.data_len, block_a.data.len());
    assert_eq!(block_a.header.codec, Codec::None);
    assert_eq!(block_a.data, (1..=40).collect::<Vec<u8>>());
    assert!(block_a.header.has_next);
    let block_b = block_b.unwrap();
    assert_eq!(block_b.header.data_len, 60);
    assert_eq!(block_b.header.data_len, block_b.data.len());
    assert_eq!(block_b.header.codec, Codec::None);
    assert_eq!(block_b.data, (41..=100).collect::<Vec<u8>>());
    assert!(!block_b.header.has_next);
}

#[rstest]
fn test_separate_atom(composer: Composer, data: MarkedData) {
    let (block_a, block_b) = composer.separate(data.clone(), 1000).unwrap();
    assert_eq!(block_a.header, data.header);
    assert_eq!(block_a.data, data.data);
    assert!(!block_a.header.has_next);
    assert!(block_b.is_none())
}

#[rstest]
fn test_join_ok(composer: Composer, data: MarkedData) {
    let (block_a, block_b) = composer.separate(data.clone(), 50).unwrap();
    let block_b = block_b.unwrap();
    let result = composer.compose(vec![block_a, block_b]).unwrap();
    assert_eq!(data.header, result.header);
    assert_eq!(data.data, result.data);
}

#[rstest]
fn test_join_err(composer: Composer, data: MarkedData) {
    let (block_a, block_b) = composer.separate(data.clone(), 50).unwrap();
    let mut block_b = block_b.unwrap();
    block_b.header.codec = Codec::Brotli;
    let err = match composer.compose(vec![block_a, block_b]) {
        Ok(_) => panic!(),
        Err(val) => val,
    };
    assert_eq!(err, HIPError::MergingDifferentCodecs);
}
