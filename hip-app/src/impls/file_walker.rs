use std::fs::{metadata, read_dir, File};
use std::io::{self, Read, Write};
use std::path::{Path, PathBuf};

use crate::proto::common::IFileWalker;

pub struct FileWalker {}

impl FileWalker {
    pub fn new() -> FileWalker {
        FileWalker {}
    }
}

impl IFileWalker for FileWalker {
    fn get_all_files(&self, base_dir: &Path) -> io::Result<Vec<PathBuf>> {
        let meta = metadata(base_dir)?;
        if meta.is_file() {
            return Ok(vec![base_dir.to_path_buf()]);
        }
        let mut result = Vec::new();
        for entry in read_dir(base_dir)? {
            let item = entry?.path();
            let meta = metadata(&item)?;
            if !meta.is_dir() {
                result.push(item);
            }
        }
        Ok(result)
    }

    fn read_file(&self, path: &Path) -> io::Result<Vec<u8>> {
        let mut buffer = Vec::new();
        File::open(path)?.read_to_end(&mut buffer)?;
        Ok(buffer)
    }

    fn write_file(&self, path: &Path, data: &[u8]) -> io::Result<()> {
        let mut file = File::create(path)?;
        file.write_all(data)
    }

    fn create_dir(&self, path: &Path) -> io::Result<()> {
        std::fs::create_dir_all(path)
    }
}
