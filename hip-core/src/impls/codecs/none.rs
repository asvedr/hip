use crate::entities::common::Codec as C;
use crate::proto::common::ICodec;
use crate::CodecError;

pub struct Codec {}

impl ICodec for Codec {
    fn slug(&self) -> C {
        C::None
    }

    fn encode(&self, data: &[u8]) -> Result<Vec<u8>, CodecError> {
        Ok(data.to_vec())
    }

    fn decode(&self, data: &[u8]) -> Result<Vec<u8>, CodecError> {
        Ok(data.to_vec())
    }
}
