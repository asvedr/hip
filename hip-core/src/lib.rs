mod entities;
mod impls;
mod proto;

pub use entities::common::{DataHeader, MarkedData};
pub use entities::errors::*;

pub use proto::common::{IComposer, IImagePack, IPreparer};

pub fn create_preparer() -> Box<dyn IPreparer> {
    Box::new(impls::preparer::Preparer {
        codecs: impls::codecs::all(),
    })
}

pub fn create_composer() -> Box<dyn IComposer> {
    Box::new(impls::composer::Composer {})
}

pub fn create_image_pack() -> Box<dyn IImagePack> {
    Box::new(impls::image_pack::ImagePack::new())
}
