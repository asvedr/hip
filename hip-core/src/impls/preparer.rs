use crate::entities::common::{DataHeader, MarkedData};
use crate::proto::common::{ICodec, IPreparer};
use crate::{CodecError, HIPError};

pub struct Preparer {
    pub codecs: Vec<Box<dyn ICodec>>,
}

impl Preparer {
    fn decode_with_codec(&self, codec: &dyn ICodec, data: &[u8]) -> Result<Vec<u8>, HIPError> {
        match codec.decode(data) {
            Ok(val) => Ok(val),
            Err(CodecError::CanNotDecode(msg)) => Err(HIPError::CanNotDecode(format!(
                "codec({:?}): {}",
                codec.slug(),
                msg
            ))),
            Err(_) => unreachable!(),
        }
    }
}

impl IPreparer for Preparer {
    fn prepare(&self, data: &[u8], bits_used: usize) -> MarkedData {
        let mut codec_to_size = Vec::new();
        for codec in self.codecs.iter() {
            let encoded = match codec.encode(data) {
                Ok(val) => val,
                Err(_) => continue,
            };
            codec_to_size.push((codec.slug(), encoded));
        }
        let (codec, encoded) = codec_to_size
            .into_iter()
            .min_by_key(|(_, res)| res.len())
            .unwrap();
        let header = DataHeader {
            bits_used,
            has_next: false,
            data_len: encoded.len(),
            codec,
        };
        MarkedData {
            header,
            data: encoded,
        }
    }

    fn decode(&self, data: MarkedData) -> Result<Vec<u8>, HIPError> {
        for codec in self.codecs.iter() {
            if codec.slug() == data.header.codec {
                return self.decode_with_codec(&**codec, &data.data);
            }
        }
        let msg = format!(
            "codec({:?}) recognized but not supported",
            data.header.codec
        );
        Err(HIPError::UnknownCodec(msg))
    }
}
