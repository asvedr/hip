use image::{
    Bgr, Bgra, DynamicImage, GenericImageView, ImageBuffer, Luma, LumaA, Pixel, Rgb, Rgba,
};
use std::ops::{BitAnd, BitOr, BitXor};

use crate::entities::common::{DataHeader, MarkedData};
use crate::entities::errors::HIPError;
use crate::proto::common::IImagePack;
use crate::proto::pixels::PixelContainer;

pub struct ImagePack {
    zero_masks: [u8; BITS_IN_BYTE],
}

#[derive(Default, Debug)]
struct Ptr {
    byte: usize,
    bit: usize,
}

impl Ptr {
    fn inc(&mut self, limit: usize) {
        let new_bit = self.bit + 1;
        if new_bit >= limit {
            self.byte += 1;
            self.bit = 0;
        } else {
            self.bit = new_bit;
        }
    }
}

const BITS_IN_BYTE: usize = 8;
const MASKS: [u8; BITS_IN_BYTE] = [
    u8::from_be(0b0000_0001),
    u8::from_be(0b0000_0010),
    u8::from_be(0b0000_0100),
    u8::from_be(0b0000_1000),
    u8::from_be(0b0001_0000),
    u8::from_be(0b0010_0000),
    u8::from_be(0b0100_0000),
    u8::from_be(0b1000_0000),
];

impl ImagePack {
    pub fn new() -> ImagePack {
        let zero_masks = MASKS
            .iter()
            .map(|byte| byte.bitxor(0b1111_1111))
            .collect::<Vec<_>>();
        ImagePack {
            zero_masks: zero_masks.try_into().unwrap(),
        }
    }

    fn pack_bits(&self, container: &mut [u8], byte: u8, ptr: &mut Ptr, bit_limit: usize) {
        for mask in MASKS {
            let src = container[ptr.byte];
            if byte.bitand(mask) != 0 {
                container[ptr.byte] = src.bitor(MASKS[ptr.bit]);
            } else {
                container[ptr.byte] = src.bitand(self.zero_masks[ptr.bit]);
            }
            ptr.inc(bit_limit)
        }
    }

    fn pack_bytes(&self, container: &mut [u8], data: &[u8], ptr: &mut Ptr, bit_limit: usize) {
        for byte in data {
            self.pack_bits(container, *byte, ptr, bit_limit)
        }
    }

    fn pack_static_pic<P: Pixel<Subpixel = u8> + 'static + PixelContainer>(
        &self,
        container: ImageBuffer<P, Vec<u8>>,
        marked_data: &MarkedData,
    ) -> ImageBuffer<P, Vec<u8>> {
        let (w, h) = container.dimensions();
        let mut bytes = container.into_raw();
        let mut ptr = Ptr::default();
        self.pack_bytes(&mut bytes, &marked_data.header.to_bytes(), &mut ptr, 1);
        self.pack_bytes(
            &mut bytes,
            &marked_data.data,
            &mut ptr,
            marked_data.header.bits_used,
        );
        ImageBuffer::<P, Vec<u8>>::from_raw(w, h, bytes).unwrap()
    }

    fn unpack_header(&self, bytes: &[u8]) -> Result<DataHeader, HIPError> {
        let len = DataHeader::len();
        if bytes.len() < len * BITS_IN_BYTE {
            return Err(HIPError::UnexpectedEOF);
        }
        let result = self.unpack_bytes(bytes, len, 1);
        match DataHeader::from_bytes(&result) {
            Some(val) => Ok(val),
            _ => Err(HIPError::InvalidHeader),
        }
    }

    fn unpack_bit(&self, bytes: &[u8], ptr: &mut Ptr, bit_used: usize) -> u8 {
        let byte = bytes[ptr.byte];
        let val = (byte.bitand(MASKS[ptr.bit]) != 0) as u8;
        ptr.inc(bit_used);
        val
    }

    fn unpack_bytes(&self, bytes: &[u8], data_len: usize, bit_limit: usize) -> Vec<u8> {
        let mut ptr = Ptr::default();
        let mut result = Vec::with_capacity(data_len);
        for _ in 0..data_len {
            let mut byte = 0;
            for j in 0..BITS_IN_BYTE {
                let val = self.unpack_bit(bytes, &mut ptr, bit_limit);
                byte = byte.bitor((2_u8.pow(j as u32) * val).to_be());
            }
            result.push(byte);
        }
        result
    }

    fn unpack_data(&self, header: &DataHeader, bytes: &[u8]) -> Result<Vec<u8>, HIPError> {
        if bytes.len() * header.bits_used < header.data_len * 8 {
            return Err(HIPError::UnexpectedEOF);
        }
        let result = self.unpack_bytes(bytes, header.data_len, header.bits_used);
        Ok(result)
    }

    fn unpack_static_pic<P: Pixel<Subpixel = u8> + 'static + PixelContainer>(
        &self,
        container: ImageBuffer<P, Vec<u8>>,
    ) -> Result<MarkedData, HIPError> {
        let bytes = container.into_raw();
        let header_len = DataHeader::len() * BITS_IN_BYTE;
        let header = self.unpack_header(&bytes[..header_len])?;
        if header.bits_used > BITS_IN_BYTE {
            return Err(HIPError::InvalidBitNumber);
        }
        let data = self.unpack_data(&header, &bytes[header_len..])?;
        Ok(MarkedData { header, data })
    }

    fn scan_header_static_pic<P: Pixel<Subpixel = u8> + 'static + PixelContainer>(
        &self,
        container: ImageBuffer<P, Vec<u8>>,
    ) -> Option<DataHeader> {
        let bytes = container.into_raw();
        let header_len = DataHeader::len() * BITS_IN_BYTE;
        self.unpack_header(&bytes[..header_len]).ok()
    }
}

impl IImagePack for ImagePack {
    fn get_picture_bytes_capacity(&self, image: &DynamicImage, bits_used: usize) -> usize {
        let (w, h) = image.dimensions();
        let channels = match image {
            DynamicImage::ImageLuma8(_) => Luma::CAPACITY,
            DynamicImage::ImageLumaA8(_) => LumaA::CAPACITY,
            DynamicImage::ImageRgb8(_) => Rgb::CAPACITY,
            DynamicImage::ImageRgba8(_) => Rgba::CAPACITY,
            DynamicImage::ImageBgra8(_) => Bgra::CAPACITY,
            DynamicImage::ImageBgr8(_) => Bgr::CAPACITY,
        };
        let total = ((w * h) as usize * channels) / BITS_IN_BYTE;
        let without_header = total - DataHeader::len();
        DataHeader::len() + (without_header * bits_used)
    }

    fn pack(&self, image: DynamicImage, data: &MarkedData) -> Result<DynamicImage, HIPError> {
        if data.header.bits_used > BITS_IN_BYTE {
            return Err(HIPError::InvalidBitNumber);
        }
        let capacity = self.get_picture_bytes_capacity(&image, data.header.bits_used);
        let required = data.len();
        if required > capacity {
            return Err(HIPError::ContainerTooSmall);
        }
        let new_image = match image {
            DynamicImage::ImageLuma8(img) => {
                DynamicImage::ImageLuma8(self.pack_static_pic(img, data))
            }
            DynamicImage::ImageLumaA8(img) => {
                DynamicImage::ImageLumaA8(self.pack_static_pic(img, data))
            }
            DynamicImage::ImageRgb8(img) => {
                DynamicImage::ImageRgb8(self.pack_static_pic(img, data))
            }
            DynamicImage::ImageRgba8(img) => {
                DynamicImage::ImageRgba8(self.pack_static_pic(img, data))
            }
            DynamicImage::ImageBgra8(img) => {
                DynamicImage::ImageBgra8(self.pack_static_pic(img, data))
            }
            DynamicImage::ImageBgr8(img) => {
                DynamicImage::ImageBgr8(self.pack_static_pic(img, data))
            }
        };
        Ok(new_image)
    }

    fn unpack(&self, image: DynamicImage) -> Result<MarkedData, HIPError> {
        match image {
            DynamicImage::ImageLuma8(img) => self.unpack_static_pic(img),
            DynamicImage::ImageLumaA8(img) => self.unpack_static_pic(img),
            DynamicImage::ImageRgb8(img) => self.unpack_static_pic(img),
            DynamicImage::ImageRgba8(img) => self.unpack_static_pic(img),
            DynamicImage::ImageBgra8(img) => self.unpack_static_pic(img),
            DynamicImage::ImageBgr8(img) => self.unpack_static_pic(img),
        }
    }

    fn scan_header(&self, image: DynamicImage) -> Option<DataHeader> {
        match image {
            DynamicImage::ImageLuma8(img) => self.scan_header_static_pic(img),
            DynamicImage::ImageLumaA8(img) => self.scan_header_static_pic(img),
            DynamicImage::ImageRgb8(img) => self.scan_header_static_pic(img),
            DynamicImage::ImageRgba8(img) => self.scan_header_static_pic(img),
            DynamicImage::ImageBgra8(img) => self.scan_header_static_pic(img),
            DynamicImage::ImageBgr8(img) => self.scan_header_static_pic(img),
        }
    }
}
