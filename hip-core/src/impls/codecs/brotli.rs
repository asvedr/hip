use crate::CodecError;
use brotli::{CompressorWriter, Decompressor};
use std::io::{Read, Write};

use crate::entities::common::Codec as C;
use crate::proto::common::ICodec;

pub struct Codec {}

const BUFFER_SIZE: usize = 4096;
const QUALITY: u32 = 11;
const LG_WINDOW_SIZE: u32 = 20;

impl ICodec for Codec {
    fn slug(&self) -> C {
        C::Brotli
    }

    fn encode(&self, data: &[u8]) -> Result<Vec<u8>, CodecError> {
        let mut result: Vec<u8> = Vec::new();
        {
            let mut compressor =
                CompressorWriter::new(&mut result, BUFFER_SIZE, QUALITY, LG_WINDOW_SIZE);
            compressor
                .write_all(data)
                .map_err(|err| CodecError::CodecFailed(err.to_string()))?;
        }
        Ok(result)
    }

    fn decode(&self, data: &[u8]) -> Result<Vec<u8>, CodecError> {
        let mut dec = Decompressor::new(data, data.len());
        let mut buffer = Vec::with_capacity(data.len());
        dec.read_to_end(&mut buffer)
            .map_err(|err| CodecError::CanNotDecode(err.to_string()))?;
        Ok(buffer)
    }
}
