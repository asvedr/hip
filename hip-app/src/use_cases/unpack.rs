use clidi::cli::validators::AsIsValidator;
use clidi::entities::{CliUseCaseMeta, Param, RawUseCaseRequest, UseCaseError};
use clidi::proto::cli::ICliUseCase;
use clidi::proto::di::IUseCase;
use hip_core::{IComposer, IImagePack, IPreparer, MarkedData};
use image::open;
use std::path::{Path, PathBuf};

use crate::proto::common::IFileWalker;

pub struct UnpackUseCase {
    file_walker: Box<dyn IFileWalker>,
    hip_preparer: Box<dyn IPreparer>,
    hip_composer: Box<dyn IComposer>,
    hip_pack: Box<dyn IImagePack>,
}

pub struct Request {
    imgs_path: PathBuf,
    out_path: PathBuf,
}

impl UnpackUseCase {
    pub fn new(
        file_walker: Box<dyn IFileWalker>,
        hip_preparer: Box<dyn IPreparer>,
        hip_composer: Box<dyn IComposer>,
        hip_pack: Box<dyn IImagePack>,
    ) -> UnpackUseCase {
        Self {
            file_walker,
            hip_preparer,
            hip_composer,
            hip_pack,
        }
    }

    fn unpack_part(&self, path: &Path) -> Result<MarkedData, UseCaseError> {
        let img = open(path).map_err(UseCaseError::make_runtime)?;
        self.hip_pack
            .unpack(img)
            .map_err(UseCaseError::make_runtime)
    }
}

impl IUseCase for UnpackUseCase {
    type Request = Request;
    type Response = ();

    fn execute(&mut self, request: Self::Request) -> Result<Self::Response, UseCaseError> {
        let img_paths = self
            .file_walker
            .get_all_files_sorted(&request.imgs_path)
            .map_err(UseCaseError::make_runtime)?;
        let mut parts = Vec::new();
        for img in img_paths {
            let part = self.unpack_part(&img)?;
            let has_next = part.has_next();
            parts.push(part);
            if !has_next {
                break;
            }
        }
        let composed = self
            .hip_composer
            .compose(parts)
            .map_err(UseCaseError::make_runtime)?;
        let data = self
            .hip_preparer
            .decode(composed)
            .map_err(UseCaseError::make_runtime)?;
        self.file_walker
            .write_file(&request.out_path, &data)
            .map_err(UseCaseError::make_runtime)?;
        println!("success");
        Ok(())
    }
}

impl ICliUseCase for UnpackUseCase {
    fn get_meta(&self) -> CliUseCaseMeta {
        CliUseCaseMeta::new("unpack data from image(s)")
            .param(Param::new::<_, AsIsValidator>("imgs").short("i"))
            .param(Param::new::<_, AsIsValidator>("out").short("o"))
    }

    fn validate_request(&self, raw: RawUseCaseRequest) -> Self::Request {
        Request {
            imgs_path: PathBuf::from(raw.get::<String>("imgs")),
            out_path: PathBuf::from(raw.get::<String>("out")),
        }
    }
}
